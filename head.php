<!DOCTYPE html>
<html lang="<?php echo LANG ?>" xml:lang="<?php echo LANG ?>">
<head>
  	<meta name="description" content="<?php echo METADESC ?>">
    <title><?php echo MAINTITLE ?></title>
    
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<meta creator="Nicolas Wadoux">
	<meta charset="utf-8">
	<link rel="sitemap" type="application/xml" href="../sitemap.xml">
	<link rel="stylesheet" type="text/css" href="../ressources/bootstrap/css/bootstrap.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="../ressources/css/style.css">
		
</head>