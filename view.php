<body class="overflow-hidden">
	<section id="loader">
		<div id="loader-container">
			<span>N</span>
			<span>W</span>
		</div>
	</section>
	<main>
		<header class="header">
			<nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top navbar-custom">
			  <div class="container-fluid">
			    <a class="navbar-brand"  data-nw-href="#homeScreen" role="button">NW</a>
			    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			      <span class="navbar-toggler-icon"></span>
			    </button>
			    <div class="collapse navbar-collapse" id="navbarSupportedContent">
			      <ul class="navbar-nav ms-auto">
			        <li class='nav-item'>
						<a data-nw-href="#aboutScreen" class="nav-link" role="button"><?php echo ABOUTME ?></a>
					</li>
					<li class='nav-item'>
						<a data-nw-href="#competencesScreen" class="nav-link" role="button"><?php echo COMPETENCES ?></a>
					</li>
					<li class='nav-item'>
						<a data-nw-href="#qualificationsScreen" class="nav-link" role="button"><?php echo QUALIFICATIONS ?></a>
					</li>
					<li class='nav-item'>
						<a data-nw-href="#portfolioScreen" class="nav-link" role="button"><?php echo PORTFOLIO ?></a>
					</li>
					<li class='nav-item'>
						<a data-nw-href="#referencesScreen" class="nav-link" role="button"><?php echo REFERENCES ?></a>
					</li>
					<li class='nav-item'>
						<a data-nw-href="#contactScreen" class="nav-link" role="button"><?php echo CONTACTME ?></a>
					</li>
					<li class='nav-item'>
						<a href="<?php echo "../ressources/download/".DLCVLINK ?>" target="_blank" class="nav-link"><?php echo CV." " ?><i class="fa fa-download"></i></a>
					</li>
			        <li class="nav-item dropdown">
			          <a class="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">	&#127757;
			            <?php echo ucfirst(LANG) ?>
			          </a>
			          <ul class="dropdown-menu" id="dropdownLang" aria-labelledby="navbarDropdown">
			            <li><a href="/"><button class="dropdown-item" name='lang' value="fr">Français</button></a></li>
			            <li><a href="../en"><button class="dropdown-item" name='lang' value="en">English</button></a></li>
			            <li><a href="../es"><button class="dropdown-item" name='lang' value="es">Español</button></a></li>
			          </ul>
			        </li>
			      </ul>
			    </div>
			  </div>
			</nav>

		</header>
		<!-- HOME -->
	  <section class='screen' >
	  	<div class='screen 2backgroundImage 2clipInvertDownRectangle' id='homeScreen'>
	  		<section id="home-paralax">
<!--	  			<img src="./ressources/images/Mont-Blanc.jpg" id='mont-blanc'>
	  			<img src="./ressources/images/parachutist.png" id='parachutist'>
 	  			<img src="./ressources/images/trees.png" id='trees'>
 -->	  		</section>
	  		<div id='homeContent'>
	  			<h1 id='home_name'>Nicolas Wadoux</h1>
	  			<p id='home_status'><?php echo HOME_STATUS ?></p>
	  		</div>
	  	</div>
	  </section>

	  <!-- About me -->
	  <section class='screen' id='aboutScreen'>
	  	<div class='spacermenu'></div>
			<h1 class='sectionTitle h1-spacer-under'><?php echo ABOUTME ?></h1>
			<div class="screenCenterContent container">
				<div class='row'>
	  			<div class="pic_container col-md-1">
	  				<div class="picContent">
			  			<span class="pic_back pic_element"></span>
			  			<div id="profil_pic" class="pic_element"></div>
	  				</div>
	  			</div>
	  			
	  			<div id="about_p_container" class='col-md'>
	  				<nav class='btn_nav'>
	  					<ul>
	  						<li data-nw-id-target='about_presentation_p' data-nw-classGroup="about_p" class="js_show_btn selected_btn"><?php echo PRESENTATION_BTN ?></li>
	  						<li data-nw-id-target='about_languages_p' data-nw-classGroup="about_p" class="js_show_btn"><?php echo LANGUAGES_BTN ?></li>
	  						<li data-nw-id-target='about_softskills_p' data-nw-classGroup="about_p" class="js_show_btn"><?php echo SOFTSKILLS_BTN ?></li>
	  						<li data-nw-id-target='about_hobbies_p' data-nw-classGroup="about_p" class="js_show_btn"><?php echo HOBBIES_BTN ?></li>
	  					</ul>
	  				</nav>
	  				<div class="box_style about_p" id="about_presentation_p">
	  					<p class='p-spacer-under'><?php echo PRESENTATION_TXT_1 ?></p>
	  					<p class='p-spacer-under'><?php echo PRESENTATION_TXT_2 ?></p>
	  					<p><?php echo PRESENTATION_TXT_3 ?></p>
	  				</div>
	  				<div class="box_style about_p hidden" id="about_languages_p">
	  					<div>
	  						<img class="flagImage" src="../ressources/images/FR.png" alt="<?php echo LANGUAGES_FR ?>">
	  						<h3><?php echo LANGUAGES_FR ?></h3>
	  						<p><?php echo LANGUAGES_FR_LVL ?></p>
	  					</div>
	  					<div>
	  						<img class="flagImage" src="../ressources/images/UK.png" alt="<?php echo LANGUAGES_EN ?>">
	  						<h3><?php echo LANGUAGES_EN ?></h3>
	  						<p><?php echo LANGUAGES_EN_LVL ?> - TOEIC 925</p>
	  					</div>
	  					<div>
	  						<img class="flagImage" src="../ressources/images/ES.png" alt="<?php echo LANGUAGES_ES ?>">
	  						<h3><?php echo LANGUAGES_ES ?></h3>
	  						<p><?php echo LANGUAGES_ES_LVL ?></p>
	  					</div>
	  				</div>
	  				<div class="box_style about_p hidden" id="about_softskills_p">
	  					<ul>
	  						<li><?php echo SOFTSKILLS_EMOTION ?></li>
	  						<li><?php echo SOFTSKILLS_RELATION ?></li>
	  						<li><?php echo SOFTSKILLS_CURIOSITY ?></li>
	  						<li><?php echo SOFTSKILLS_PONCTUALITY ?></li>
	  						<li><?php echo SOFTSKILLS_RESOLUTION ?></li>
	  					</ul>
	  				</div>
	  				<div class="box_style about_p hidden" id="about_hobbies_p">
	  					<div class='container-fluid'>
	  						<div class='row'>
	  							<h3>- <?php echo HOBBIES_VIOLIN_TILTE ?> -</h3>
	  							<p><?php echo HOBBIES_VIOLIN_1 ?></p>
	  							<p class='p-spacer-under'><?php echo HOBBIES_VIOLIN_2 ?></p>
	  						</div>
	  						<div class='row'>
	  							<h3>- <?php echo HOBBIES_TRAVEL_TITLE ?> -</h3>
	  							<p class='p-spacer-under'><?php echo HOBBIES_TRAVEL ?></p>
	  						</div>
	  						<div class='row'>
	  							<h3>- <?php echo HOBBIES_COOKING_TITLE ?> -</h3>
	  							<p><?php echo HOBBIES_COOKING ?></p>
	  						</div>
	  					</div>
	  				</div>
	  			</div>
				</div>
			</div>
	  </section>

	  	<!-- Competences -->
	  <section class='screen'>
			<div class='screen clipRectangles' id='competencesScreen'>
				<div class='spacermenu'></div>
				<h1 class='h1WhiteBefAft sectionTitle h1-spacer-under'><?php echo COMPETENCES ?></h1>
				<div class="screenCenterContent container-fluid max-w-1400">
					<div class='row'>
						<div class='col-md-3 col-sm-6'>
	  					<div class='competencesCard'>
	  						<div class='competencesCard-header'>
	  							<h2><?php echo WEB ?></h2>
	  						</div>
	  						<div>
	  							<div class='progressContainer'>
	  								<p class='progressLabel'>HTML / CSS / JS</p>
		  							<div class="progress">
		  								<div class='progress-bar' role='progressbar' aria-valuemin="0" aria-valuemax="100">85 %</div>
		  							</div>
	  							</div>
	  							<div class='progressContainer'>
	  								<p class='progressLabel'>PHP</p>
		  							<div class="progress">
		  								<div class='progress-bar' role='progressbar' aria-valuemin="0" aria-valuemax="100">80 %</div>
		  							</div>
	  							</div>
	  							<div class='progressContainer'>
	  								<p class='progressLabel'>Angular</p>
		  							<div class="progress">
		  								<div class='progress-bar' role='progressbar' aria-valuemin="0" aria-valuemax="100">75 %</div>
		  							</div>
	  							</div>
	  							<div class='progressContainer'>
	  								<p class='progressLabel'>Laravel / Symfony</p>
		  							<div class="progress">
		  								<div class='progress-bar' role='progressbar' aria-valuemin="0" aria-valuemax="100">75 %</div>
		  							</div>
	  							</div>
	  							<div class='progressContainer'>
	  								<p class='progressLabel'>NodeJs</p>
		  							<div class="progress">
		  								<div class='progress-bar' role='progressbar' aria-valuemin="0" aria-valuemax="100">70 %</div>
		  							</div>
	  							</div>
	  							<div class='progressContainer'>
	  								<p class='progressLabel'>WordPress</p>
		  							<div class="progress">
		  								<div class='progress-bar' role='progressbar' aria-valuemin="0" aria-valuemax="100">50 %</div>
		  							</div>
	  							</div>
	  						</div>
							</div>
						</div>
						<div class='col-md-3 col-sm-6'>
	  					<div class='competencesCard'>
	  						<div class='competencesCard-header'>
	  							<h2><?php echo DB ?></h2>
	  						</div>
	  						<div>
	  							<div class='progressContainer'>
	  								<p class='progressLabel'>MySQL</p>
		  							<div class="progress">
		  								<div class='progress-bar' role='progressbar' aria-valuemin="0" aria-valuemax="100">85 %</div>
		  							</div>
	  							</div>
	  							<div class='progressContainer'>
	  								<p class='progressLabel'>PgSQL</p>
		  							<div class="progress">
		  								<div class='progress-bar' role='progressbar' aria-valuemin="0" aria-valuemax="100">80 %</div>
		  							</div>
	  							</div>
	  							<div class='progressContainer'>
	  								<p class='progressLabel'>PL/SQL</p>
		  							<div class="progress">
		  								<div class='progress-bar' role='progressbar' aria-valuemin="0" aria-valuemax="100">60 %</div>
		  							</div>
	  							</div>
	  							<div class='progressContainer'>
	  								<p class='progressLabel'>Oracle</p>
		  							<div class="progress">
		  								<div class='progress-bar' role='progressbar' aria-valuemin="0" aria-valuemax="100">80 %</div>
		  							</div>
	  							</div>
	  							<div class='progressContainer'>
	  								<p class='progressLabel'>PDO</p>
		  							<div class="progress">
		  								<div class='progress-bar' role='progressbar' aria-valuemin="0" aria-valuemax="100">90 %</div>
		  							</div>
	  							</div>
	  							<div class='progressContainer'>
	  								<p class='progressLabel'>Merise</p>
		  							<div class="progress">
		  								<div class='progress-bar' role='progressbar' aria-valuemin="0" aria-valuemax="100">85 %</div>
		  							</div>
	  							</div>
	  						</div>
							</div>
						</div>
						<div class='col-md-3 col-sm-6'>
							<div class='competencesCard'>
	  						<div class='competencesCard-header'>
	  							<h2><?php echo PROGRAMMING ?></h2>
	  						</div>
	  						<div>
	  							<div class='progressContainer'>
	  								<p class='progressLabel'>C#</p>
		  							<div class="progress">
		  								<div class='progress-bar' role='progressbar' aria-valuemin="0" aria-valuemax="100">70 %</div>
		  							</div>
	  							</div>
	  							<div class='progressContainer'>
	  								<p class='progressLabel'>Kotlin (Android)</p>
		  							<div class="progress">
		  								<div class='progress-bar' role='progressbar' aria-valuemin="0" aria-valuemax="100">70 %</div>
		  							</div>
	  							</div>
	  							<div class='progressContainer'>
	  								<p class='progressLabel'>Java</p>
		  							<div class="progress">
		  								<div class='progress-bar' role='progressbar' aria-valuemin="0" aria-valuemax="100">65 %</div>
		  							</div>
	  							</div>
	  							<div class='progressContainer'>
	  								<p class='progressLabel'>Flutter</p>
		  							<div class="progress">
		  								<div class='progress-bar' role='progressbar' aria-valuemin="0" aria-valuemax="100">55 %</div>
		  							</div>
	  							</div>
	  							<div class='progressContainer'>
	  								<p class='progressLabel'>UML</p>
		  							<div class="progress">
		  								<div class='progress-bar' role='progressbar' aria-valuemin="0" aria-valuemax="100">85 %</div>
		  							</div>
	  							</div>
	  							<div class='progressContainer'>
	  								<p class='progressLabel'>Python</p>
		  							<div class="progress">
		  								<div class='progress-bar' role='progressbar' aria-valuemin="0" aria-valuemax="100">50 %</div>
		  							</div>
	  							</div>
	  						</div>
	  					</div>
						</div>
						<div class='col-md-3 col-sm-6'>
							<div class='competencesCard'>
	  						<div class='competencesCard-header'>
	  							<h2><?php echo OTHERS ?></h2>
	  						</div>
	  						<div>
	  							<div class='progressContainer'>
	  								<p class='progressLabel'><?php echo GDPR ?></p>
		  							<div class="progress">
		  								<div class='progress-bar' role='progressbar' aria-valuemin="0" aria-valuemax="100">70 %</div>
		  							</div>
	  							</div>
	  							<div class='progressContainer'>
	  								<p class='progressLabel'>Odoo</p>
		  							<div class="progress">
		  								<div class='progress-bar' role='progressbar' aria-valuemin="0" aria-valuemax="100">65 %</div>
		  							</div>
	  							</div>
	  							<div class='progressContainer'>
	  								<p class='progressLabel'>Adobe XD</p>
		  							<div class="progress">
		  								<div class='progress-bar' role='progressbar' aria-valuemin="0" aria-valuemax="100">75 %</div>
		  							</div>
	  							</div>
	  							<div class='progressContainer'>
	  								<p class='progressLabel'>Scrum</p>
		  							<div class="progress">
		  								<div class='progress-bar' role='progressbar' aria-valuemin="0" aria-valuemax="100">85 %</div>
		  							</div>
	  							</div>
	  							<div class='progressContainer'>
	  								<p class='progressLabel'>Photoshop</p>
		  							<div class="progress">
		  								<div class='progress-bar' role='progressbar' aria-valuemin="0" aria-valuemax="100">50 %</div>
		  							</div>
	  							</div>
	  							<div class='progressContainer'>
	  								<p class='progressLabel'>Git</p>
		  							<div class="progress">
		  								<div class='progress-bar' role='progressbar' aria-valuemin="0" aria-valuemax="100">70 %</div>
		  							</div>
	  							</div>
	  						</div>
	  					</div>
						</div>
					</div>
				</div>
			</div>	
	  </section>

	  	<!-- Qualifications -->
	  <section class='screen' id='qualificationsScreen'>
	  	<div class='spacermenu'></div>
	  	<h1 class='sectionTitle'><?php echo QUALIFICATIONS ?></h1>
	  	<div id='qualifContent'>
	  		<span class='timeLine'></span>
	  		<div class="qualif fade-in-out">
	  			<h2><?php echo LP_DIM ?></h2>
	  			<span class="portfolioCard-spacer"></span>
	  			<div class="qualifText">
	  				<p><?php echo DIM ?></p>
	  				<p><?php echo DIM_UNIV ?></p>
	  			</div>
	  			<p class="qualifDate">2021 - 2022</p>
	  		</div>
	  		<div class="qualif fade-in-out">
	  			<h2><?php echo DUT ?></h2>
	  			<span class="portfolioCard-spacer"></span>
	  			<div class="qualifText">
	  				<p><?php echo DUT_INFO ?></p>
	  				<p><?php echo DUT_UNIV ?></p>
	  			</div>
	  		  	<p class="qualifDate odd">2019 - 2021</p>
			</div>
	  		<div class="qualif fade-in-out">
	  			<h2><?php echo BAC_S ?></h2>
	  			<span class="portfolioCard-spacer"></span>
	  			<div class="qualifText">
	  				<p><?php echo BAC_S_DESC ?></p>
	  				<p><?php echo BAC_OPT ?></p>
	  				<p><?php echo BAC_UNIV ?></p>
	  			</div>
	  			<p class="qualifDate">2016 - 2019</p>
	  		</div>
	  	</div>
	  </section>

	  	<!-- Portfolio -->
	  <section class='screen'>
	  	<div class='screen clipRectangles' id='portfolioScreen'>
	  		<div class='spacermenu'></div>
			<h1 class='h1WhiteBefAft sectionTitle'><?php echo PORTFOLIO ?></h1>
			<div class='container-fluid'>
				<div class='row'>
					<nav class='btn_nav' id='portfolioNav'>
	  					<ul>
	  						<li data-nw-id-target='portfolioPerso' data-nw-classGroup="portfolioGroup" class="js_show_btn"><?php echo PERSONAL ?></li>
	  						<li data-nw-id-target='portfolioPro' data-nw-classGroup="portfolioGroup" class="js_show_btn selected_btn portfolioPro"><?php echo PROFESSIONAL ?></li>
	  					</ul>
	  				</nav>
				</div>
				<div class='row hidden portfolioGroup screenCenterContent max-w-1400' id='portfolioPerso'>
					<div class='col-md col-sm'>
	  					<div class='portfolioCard'>
	  						<div class='portfolioCard-header'>
	  							<h2><?php echo TOQUES_HEADER ?></h2>
	  						</div>
	  						<span class='portfolioCard-spacer'></span>
	  						<div class='portfolioCard-content'>
	  							<div class="flip-image">
	  								<img src="../ressources/images/Toquerie.svg"  alt="<?php echo TOQUES_ALT ?>">
	  							</div>
	  							<div class="flip-content">
	  								<div>
		  								<p class="p-spacer-under"><?php echo TOQUES_DATE ?></p>
		  								<p class="p-spacer-under"><?php echo TOQUES_CONTENT_P1 ?></p>
		  								<p><?php echo TOQUES_CONTENT_P2 ?></p>
	  								</div>
	  							</div>
	  						</div>
	  						<span class='portfolioCard-spacer'></span>
	  						<div class='portfolioCard-footer'>
	  							<ul class='portfolioCard-footer-list'>
	  								<li class='jsCaps'>Angular</li>
	  								<li class='phpCaps'>API Laravel</li>
	  								<li class='sqlCaps'>SQL</li>
	  								<li class='wpCaps'>RGPD</li>
	  							</ul>
	  						</div>
	  					</div>
	  				</div>
					<div class='col-md col-sm'>
	  					<div class='portfolioCard'>
	  						<div class='portfolioCard-header'>
	  							<h2><?php echo PORTFOLIO2_HEADER ?></h2>
	  						</div>
	  						<span class='portfolioCard-spacer'></span>
	  						<div class='portfolioCard-content'>
	  							<div class="flip-image">
	  								<img src="../ressources/images/portfolio2.PNG" alt="<?php echo PORTFOLIO2_ALT ?>">
	  							</div>
	  							<div class="flip-content">
	  								<div>
		  								<p class="p-spacer-under"><?php echo PORTFOLIO2_DATE ?></p>
		  								<p class="p-spacer-under"><?php echo PORTFOLIO2_CONTENT_P1 ?></p>
		  								<p class="p-spacer-under"><?php echo PORTFOLIO2_CONTENT_P2 ?></p>
		  								<p class="p-spacer-under"><?php echo PORTFOLIO2_CONTENT_P3 ?></p>
		  								<p><a href="https://xd.adobe.com/view/75b20a3e-01e2-49a2-a859-fcd523e37cd0-20e1/" rel="noopener" target="_blank">- <?php echo PORTFOLIO2_LINK ?> -</a></p>
	  								</div>
	  							</div>
	  						</div>
	  						<span class='portfolioCard-spacer'></span>
	  						<div class='portfolioCard-footer'>
	  							<ul class='portfolioCard-footer-list'>
	  								<li class='htmlCaps'>HTML/CSS</li>
	  								<li class='phpCaps'>PHP</li>
	  								<li class='jsCaps'>JS</li>
	  								<li class='wpCaps'>BootStrap</li>
	  							</ul>
	  						</div>
	  					</div>
	  				</div>
	  				<div class='col-md col-sm'>
	  					<div class='portfolioCard'>
	  						<div class='portfolioCard-header'>
	  							<h2><?php echo ETUCUISINE_HEADER ?></h2>
	  						</div>
	  						<span class='portfolioCard-spacer'></span>
	  						<div class='portfolioCard-content'>
	  							<div class="flip-image">
	  								<img src="../ressources/images/etucuisine.PNG" alt="<?php echo ETUCUISINE_ALT ?>">
	  							</div>
	  							<div class="flip-content">
	  								<div>
		  								<p class="p-spacer-under">11/2019</p>
		  								<p class="p-spacer-under"><?php echo ETUCUISINE_CONTENT_P1 ?></p>
		  								<p class="p-spacer-under"><?php echo ETUCUISINE_CONTENT_P2 ?></p>
	  								</div>
	  							</div>
	  						</div>
	  						<span class='portfolioCard-spacer'></span>
	  						<div class='portfolioCard-footer'>
	  							<ul class='portfolioCard-footer-list'>
	  								<li class='htmlCaps'>HTML/CSS</li>
	  								<li class='phpCaps'>PHP</li>
	  								<li class='sqlCaps'>SQL</li>
	  							</ul>
	  						</div>
	  					</div>
	  				</div>
				</div>
				<div class='row portfolioGroup screenCenterContent max-w-1400' id='portfolioPro'>
					<div class='col-md col-sm'>
	  					<div class='portfolioCard'>
	  						<div class='portfolioCard-header'>
	  							<h2><?php echo INFOMANIAK_HEADER ?></h2>
	  						</div>
	  						<span class='portfolioCard-spacer'></span>
	  						<div class='portfolioCard-content'>
	  							<div class="flip-image">
	  								<img id="infomaniakPic" src="../ressources/images/infomaniak.png" alt="<?php echo INFOMANIAK_ALT ?>">
	  							</div>
	  							<div class="flip-content">
	  								<div>
		  								<p class="p-spacer-under"><?php echo INFOMANIAK_DATE ?></p>
		  								<p class="p-spacer-under"><?php echo INFOMANIAK_CONTENT_1 ?></p>
		  								<p class="p-spacer-under"><?php echo INFOMANIAK_CONTENT_2 ?></p>
		  								<p><?php echo INFOMANIAK_CONTENT_3 ?></p>
	  								</div>
	  							</div>
	  						</div>
	  						<span class='portfolioCard-spacer'></span>
	  						<div class='portfolioCard-footer'>
	  							<ul class='portfolioCard-footer-list'>
	  								<li class='phpCaps'>Laravel</li>
	  								<li class='jsCaps'>Angular</li>
	  								<li class='stencilCaps'>Stencil</li>
	  								<li class='wpCaps'>Scrum</li>
	  							</ul>
	  						</div>
	  					</div>
	  				</div>
					<div class='col-md col-sm'>
	  					<div class='portfolioCard'>
	  						<div class='portfolioCard-header'>
	  							<h2><?php echo KILI_HEADER ?></h2>
	  						</div>
	  						<span class='portfolioCard-spacer'></span>
	  						<div class='portfolioCard-content'>
	  							<div class="flip-image">
	  								<img src="../ressources/images/kili.PNG" alt="<?php echo KILI_ALT ?>">
	  							</div>
	  							<div class="flip-content">
	  								<div>
		  								<p class="p-spacer-under">05/2020 - 03/2021</p>
		  								<p class="p-spacer-under"><?php echo KILI_CONTENT_P1 ?></p>
		  								<p><?php echo KILI_CONTENT_P2 ?></p>
		  								<p class="p-spacer-under"><?php echo KILI_CONTENT_P3 ?></p>
		  								<p class="p-spacer-under"><?php echo KILI_CONTENT_P4 ?></p>
	  								</div>
	  							</div>
	  						</div>
	  						<span class='portfolioCard-spacer'></span>
	  						<div class='portfolioCard-footer'>
	  							<ul class='portfolioCard-footer-list'>
	  								<li class='htmlCaps'>HTML/CSS</li>
	  								<li class='phpCaps'>PHP</li>
	  								<li class='sqlCaps'>SQL</li>
	  								<li class='wpCaps'>WordPress</li>
	  							</ul>
	  						</div>
	  					</div>
	  				</div>
	  				<div class='col-md col-sm'>
	  					<div class='portfolioCard'>
	  						<div class='portfolioCard-header'>
	  							<h2><?php echo PORTFOLIO_HEADER ?></h2>
	  						</div>
	  						<span class='portfolioCard-spacer'></span>
	  						<div class='portfolioCard-content'>
	  							<div class="flip-image">
	  								<img src="../ressources/images/portfolio.PNG"  alt="<?php echo PORTFOLIO_ALT ?>">
	  							</div>
	  							<div class="flip-content">
	  								<div>
		  								<p class="p-spacer-under">11/2020</p>
		  								<p class="p-spacer-under"><?php echo PORTFOLIO_CONTENT_1 ?></p>
		  								<p class="p-spacer-under"><?php echo PORTFOLIO_CONTENT_2 ?></p>
		  								<p><a href="/portfolioV1" rel="noopener" target="_blank">- <?php echo SEEIT ?> -</a></p>
	  								</div>
	  							</div>
	  						</div>
	  						<span class='portfolioCard-spacer'></span>
	  						<div class='portfolioCard-footer'>
	  							<ul class='portfolioCard-footer-list'>
	  								<li class='htmlCaps'>HTML/CSS</li>
	  								<li class='jsCaps'><?php echo JSNATIVE ?></li>
	  							</ul>
	  						</div>
	  					</div>
	  				</div>
	  				<!-- <div class='col-md col-sm'>
	  					<div class='portfolioCard'>
	  						<div class='portfolioCard-header'>
	  							<h2><?php //echo TRIPAD_HEADER ?></h2>
	  						</div>
	  						<span class='portfolioCard-spacer'></span>
	  						<div class='portfolioCard-content'>
	  							<div class="flip-image">
	  								<img src="../ressources/images/tripadAlike.png" alt="<?php //echo TRIPAD_ALT ?>">
	  							</div>
	  							<div class="flip-content">
	  								<div>
		  								<p class="p-spacer-under">10/2020</p>
		  								<p class="p-spacer-under"><?php //echo TRIPAD_CONTENT_1 ?></p>
		  								<p><?php //echo TRIPAD_CONTENT_2 ?></p>
	  								</div>
	  							</div>
	  						</div>
	  						<span class='portfolioCard-spacer'></span>
	  						<div class='portfolioCard-footer'>
	  							<ul class='portfolioCard-footer-list'>
	  								<li class='phpCaps'>Laravel</li>
	  								<li class='wpCaps'>Scrum</li>
	  							</ul>
	  						</div>
	  					</div>
	  				</div> -->
	  				<div class='col-md col-sm'>
	  					<div class='portfolioCard'>
	  						<div class='portfolioCard-header'>
	  							<h2><?php echo ZONESKI_HEADER ?></h2>
	  						</div>
	  						<span class='portfolioCard-spacer'></span>
	  						<div class='portfolioCard-content'>
	  							<div class="flip-image">
	  								<img src="../ressources/images/logoSki.png"  alt="<?php echo ZONESKI_ALT ?>">
	  							</div>
	  							<div class="flip-content">
	  								<div>
		  								<p class="p-spacer-under"><?php echo ZONESKI_CONTENT_DATE ?></p>
		  								<p class="p-spacer-under"><?php echo ZONESKI_CONTENT_1 ?></p>
		  								<p class="p-spacer-under"><?php echo ZONESKI_CONTENT_2 ?></p>
		  								<p><?php echo ZONESKI_CONTENT_3 ?></p>
		  								<p><a href="http://dut-info-annecy.fr/ZoneSki" rel="noopener" target="_blank">- <?php echo SEEIT ?> -</a></p>
	  								</div>
	  							</div>
	  						</div>
	  						<span class='portfolioCard-spacer'></span>
	  						<div class='portfolioCard-footer'>
	  							<ul class='portfolioCard-footer-list'>
	  								<li class='htmlCaps'>HTML/CSS</li>
	  								<li class='phpCaps'>PHP</li>
	  								<li class='sqlCaps'>SQL</li>
	  							</ul>
	  						</div>
	  					</div>
	  				</div>
				</div>
			</div>
	  	</div>
	  </section>

	  	<!-- References -->
	  <section class='screen' id='referencesScreen'>
	  	<div class='spacermenu'></div>
		<h1 class='sectionTitle'><?php echo REFERENCES ?></h1>
		<div id='ref_content' class='max-w-1400 container-fluid screenCenterContent'>
			<div class='row'>
				<div class='col-sm ref_side'>
					<div class='pic_container Itemcenter'>
						<div class="picContent">
				  			<span class="pic_back pic_element"></span>
				  			<div id="first_ref_pic" class="pic_element"></div>
				  		</div>
				  	</div>
			  		<div class='ref_description Itemcenter'>
			  			<h2>Stéphanie Bouchon</h2>
			  			<p><?php echo JOB1REF ?></p>
			  			<p><?php echo WORK1REF ?></p>
			  			<p><?php echo LOC1REF ?></p>
			  			<p class='mail_ref'><a href="mailto:stephanie.bouchon@univ-savoie.fr">stephanie.bouchon@univ-savoie.fr</a></p>
			  		</div>
				</div>
				<div class='col-sm ref_side'>
					<div class='pic_container Itemcenter'>
						<div class="picContent">
				  			<span class="pic_back pic_element"></span>
				  			<div id="scdn_ref_pic" class="pic_element"></div>
						</div>
					</div>	
					<div class='ref_description Itemcenter'>
						<h2>Pascal Colin</h2>
			  			<p><?php echo JOB2REF ?></p>
			  			<p><?php echo WORK2REF ?></p>
			  			<p><?php echo LOC2REF ?></p>
			  			<p class='mail_ref'><a href="mailto:pascal.colin@univ-savoie.fr">pascal.colin@univ-savoie.fr</a></p>
			  		</div>
				</div>	
			</div>
			
		</div>
	  </section>

	  	<!-- Contact -->
	  <section class='screen'>
	  	<div class='screen clipUpRectangle' id='contactScreen'>
	  		<div class='spacermenu'></div>
	  		<h1 class='sectionTitle'><?php echo CONTACTME ?></h1>
	  		<div class='container-fluid screenCenterContent max-w-1400'>
	  			<div class='row'>
	  				<div class='col text-center' id='getintouch'>
	  					<p><?php echo GETINTOUCH_P1 ?></p> 
	  					<p><?php echo GETINTOUCH_P2 ?></p>
	  				</div>	  				
	  			</div>
	  			<div class='row'>
		  			<div class='col-sm'>
		  				<form id="contact_form" action="../mail.php" method='post'>
							<p>
								<label for="email" class='hidden'><?php echo EMAIL ?></label>
								<input class='input_contact' type="email" name="email" id="email" placeholder="<?php echo EMAIL ?>">
							</p>
							<p>
								<label for="object" class='hidden'><?php echo OBJECTMSG ?></label>
								<input class='input_contact' type="text" name="object" id="object" placeholder="<?php echo OBJECTMSG ?>">
							</p>
							<p>
								<label for="msg"><?php echo MESSAGE ?></label>
								<textarea id="msg" name="msg" rows='9'></textarea>
							</p>
							<p id='robot_check'>
								<label for='check'>5 - 1 =</label>
								<input class='input_contact' type="number" name="check" id="check">
							</p>
							<div id="sendBtnContact">
								<span class="contact_caps tenDRotated" id='bg_contact_sendBtn'></span>
								<input class="contact_caps" type="submit" id="contact_sendBtn" value="<?php echo SEND ?>"/>
							</div>	
							<div id="mail_statut">
								<p id="mail_statut_success" class="mail_alert alert-success hidden"><?php echo MAILSUCCESSFULYSENDED ?></p>
								<p id="mail_statut_error" class="mail_alert alert-danger hidden"><?php echo MAILSENDINGERROR ?></p>
							</div>
							<div>
								<p data-nw-id-target='gdpr_p' id='gdpr_btn' class="js_toggle_btn helpBtn"><?php echo PERSONALDATA ?></p>
								<p id='gdpr_p' class='hidden'>
									<?php echo GDPRCONTACT ?>
								</p>
							</div>					
						</form>

		  			</div>
		  			<div class='col-sm'>
		  				<a class='contact_caps_a' rel='noopener' href="https://www.linkedin.com/in/nicolas-wadoux-5b8271193/" target='_blank'>
		  					<div class="contact_caps clickableBtn">
			  					<img class="contact_caps_pic" src="../ressources/images/linkedin.png" alt="LinkedIn icon">
			  					<p class="contact_caps_p">Nicolas Wadoux</p>
			  				</div>
		  				</a>
		  				<a class='contact_caps_a' href="<?php echo "../ressources/download/".DLCVLINK ?>" target="_blank">
		  					<div class="contact_caps clickableBtn">
			  					<img class="contact_caps_pic" src="../ressources/images/dl.png" alt='Download icon'>
			  					<p class="contact_caps_p"><?php echo DLCV ?></p>
			  				</div>
		  				</a>
		  			</div>
		  		</div>
	  		</div>
		</div>
	  </section>
	</main>
</body>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
<script src="../ressources/bootstrap/js/bootstrap.js"></script>
<script src="../ressources/js/main.js?1"></script>
<script src="../ressources/js/mail.js"></script>
</html>

