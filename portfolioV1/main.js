//code tested on google, microsoft edge, should work on mozilla firefox and safari

window.addEventListener("load",function() {

//Cache le loader quand la page est chargée, permet le scroll (annulé par css)
	let loaderDiv=document.querySelector("#loader-div")
	let timeTransitionLoader=1000
	loaderDiv.classList.add("loaderOut")
	setTimeout(function(){loaderDiv.remove()},timeTransitionLoader)
	document.body.classList.remove('unscrollable')


/******************************/
/*Stars effect on home section*/
/******************************/
/*init*/
	let canvas = document.querySelector("#canvasStars")
	let c = canvas.getContext("2d")

	canvas.width = document.querySelector("body").clientWidth-10//bar scroll
	canvas.height = window.innerHeight

	let w = canvas.width
	let h = canvas.height

	let balls = []
	let nbBalls=20
	for(let i=0; i<nbBalls; i++)
	balls.push(
	   {
	    x: Math.random()*w,
	    y: Math.random()*h,
	    sy: -0.5,
	    r: 2+Math.random()*2,
	  })

	window.addEventListener("resize", function(){
	  refreshStars(canvas,c,balls)
	})

	setInterval(function() {
		c.clearRect(0, 0, 500000, 500000)//normalement c.clearRect(0, 0, w, h) Mais conflit avec responsive alors qu'ils sont bien changés dans refreshStars

		for(let ball of balls) {
			c.beginPath()
			c.arc(ball.x, ball.y, ball.r, 0, Math.PI*2)
			let scintillement=Math.random()
			c.fillStyle = "rgba(200,200,200,"+scintillement+")"
			c.fill()
			ball.y += ball.sy
			if(ball.y+ball.r < 0)
				ball.y=h
		}
	}, 50)

	

/*********************************/
/*Parallax style for home section*/
/*********************************/
/*
* Change legerement la position de l'image quand la souris est sur la page d'accueil
* donne un effet de mouvement pseudo parallax
*/
    let presentation = document.querySelector('#presentation_container')
    presentation.addEventListener("mousemove", function(e){
        var mouseX=(e.pageX*-1/200)
        var mouseY=(e.pageY*-1/200)
        if(window.screen.availWidth>645)
        {
            presentation.style.backgroundPosition=mouseX+'px '+mouseY+'px'
        }
            
    })


/************/
/*Navigation*/
/************/
	displayNav()
/********/
/*Konami*/
/********/
/*
* Ajoute un konami, avec un audio, quand la bonne sequence est entrée
*/
	let audio = new Audio("./ressources/audio/konami.mp3")
	let konamiTab = ['38', '38', '40', '40', '37', '39', '37', '39', '66', '65']
	let konamiIndex = 0

	document.addEventListener('keydown', function(e) {
	     let keyPressed = e.keyCode
	     let requiredKey = konamiTab[konamiIndex]

	     if (keyPressed == requiredKey) {
	       konamiIndex++
	       if (konamiIndex == konamiTab.length) {
	         konami(audio)
	         konamiIndex = 0
	       }
	     } else {
	       konamiIndex = 0
	     }
	});

	

/********/
/*Skills*/
/********/
/*
* Créé une legère animation sur les border des boutons skills (ex web design) pour inciter à clicker
*/
	let bMax=255
	let bMin=100
	let b=100
	let step=2

	for(let bd of document.querySelectorAll(".skill_part_border"))
	{
		setInterval(function(){ 
			bd.style.borderColor="rgb("+b+","+b+","+b+")"
			b+=step
			if(b>bMax)
				step=-step
			if(b<bMin)
				step=-step
		}, 100)


	}

/*
* Permet d'afficher les bons skills present sur l'HTML lors du click sur le bouton
*/
	let indexTable=["skill_web","skill_db","skill_pro"]//les differentes parties de skills

	let skillBtn = document.querySelectorAll(".skill_part")
	for(let btn of skillBtn)
	{
		btn.addEventListener("click", function(){
			//cache tout
			for(let btnRem of skillBtn)
			{
				btnRem.classList.remove("selected")
			}
			btn.classList.add("selected")

			//affiche que la partie correspondante au bouton clické (même classe)
			for(let key of indexTable)
			{
				let skilldispkey=document.querySelector("#skill_display_content").querySelector("."+key)
				if(btn.classList.contains(key))
				{
					skilldispkey.classList.remove("hidden")
					displaySkills("."+key)
				}
				else{
					skilldispkey.classList.add("hidden")
				}
			}
		})
	}
/*
* Ajout des effets lors du survol (affiche le %age)
*/
	for(let circleTxt of document.querySelectorAll(".circle_title"))
	{
		for(let txt of circleTxt.querySelectorAll("p"))
		{
			let cmin=53
			let cmax=256
			let c=53
			let step = 1
			circleTxt.addEventListener("mouseenter", function(){
				txt.classList.toggle("hidden")
			})
			circleTxt.addEventListener("mouseleave", function(){
				txt.classList.toggle("hidden")
			})
		}
	}

	displaySkills(".skill_web")


/***************/
/*About Section*/
/***************/
/*
* Gere les boutons pour l'affichage des differentes parties en html (selon classes = entre boutons et content)
*/
	let spans=document.querySelector("#select_about").querySelectorAll("span")
	let allAboutBoxes=document.querySelectorAll(".about_box")
	for(let span of spans)
	{
		span.addEventListener("click", function(){
			for(let spanRem of spans)
			{
				spanRem.classList.remove("about_selected")
			}

			for(let box of allAboutBoxes)
			{
				box.classList.add("hidden")
				if(containClassOf(span,box))//attention a l'utilisation de cette fonction
				{
					box.classList.remove("hidden")
					span.classList.add("about_selected")
				}
			}
		})
	}

	//Calcul pour avoir le nombre de jours depuis ma naissance
	let me = new Date('5/21/2001');
	let today = new Date();
	let utc1 = Date.UTC(me.getFullYear(), me.getMonth(), me.getDate());
	let utc2 = Date.UTC(today.getFullYear(), today.getMonth(), today.getDate());
	let diff= Math.floor((utc2 - utc1) / (1000 * 60 * 60 * 24));
	//calcul age
	let age = Number((today.getTime() - me.getTime()) / 31536000000).toFixed(1);//recup 1 chiffre apres virgule
	age=age.split(".")[0] //marche meme si j'ai 100ans, affiche que le nombre
	//affichage
	document.querySelector("#daysBir").innerHTML=diff+" days ago, or "+age+" years old";


/****************/
/*Qualifications*/
/****************/
/*
* Affiche des qualifications
*	Viennent du côté
*	Directement affiché si resolution insuffisante
*/
	let alreadyCalled=false
	let qual=document.querySelector("#qualifications")
	let info=document.querySelector("#dutInfo")
	let bacS=document.querySelector("#bacS")
	let cW=document.querySelector("body").clientWidth
	let maxWi=775
	if(cW>maxWi)//le cacher quand anim non lancé, .hidden ne marchait pas
	{
		info.style.opacity=0
		bacS.style.opacity=0
	}


	window.addEventListener("resize",function(){//pour réactualiser car le css n'est plus pris en compte, si jamais resize de la fenetre
		let actualW=document.querySelector("body").clientWidth
		if(actualW<=maxWi || window.innerWidth<=maxWi)
		{
			info.style.left="0%"
			bacS.style.left="0%"
		}
		else{
			info.style.left="5%"
			bacS.style.left="55%"
		}
	})
	//affichage en mouvement
	document.addEventListener("scroll",function(){
		let posY=scrollY

		if(posY>qual.offsetTop-qual.offsetHeight/2 && posY<qual.offsetTop+qual.offsetHeight/3 && alreadyCalled==false && cW>maxWi)
		{
			
			info.classList.add("mouvement")
			bacS.classList.add("mouvement")
			info.style.opacity=1
			bacS.style.opacity=1
			
			alreadyCalled=true
			let start=-50
			setInterval(function(){
				start=start+0.5
				if(start<5)
				{
					
					info.style.left=start+"%"
				}
				if(start<55)
				{
					bacS.style.left=start+"%"
				}

			},10)
		}
	})

/*************/
/*Experiences*/
/*************/
/*
* Effet fade in quand utilisateur arrive au niveau de la page
*/
	let exps=document.querySelectorAll(".card_expe")
	let expContent=document.querySelector("#experiences")
	let expAlreadyCalled=false
	let opacity=0
	for(let exp of exps)
	{
		exp.style.opacity=opacity
	}
	document.addEventListener("scroll",function(){
		if(scrollY>expContent.offsetTop-expContent.offsetHeight/2.5 && scrollY<expContent.offsetTop+expContent.offsetHeight/3 && expAlreadyCalled==false)
		{
			expAlreadyCalled=true
			for(let exp of exps)
			{
				// let i = setInterval(function(){
				// 	exp.style.opacity=opacity
				// 	opacity=opacity+0.05
				// 	if(opacity>1)
				// 		clearInterval(i)
				// }, 100)
				exp.style.opacity=1;//avec transition css c'est mieux que le faire en js comme au dessus
			}
				
			
		}
	})




/************/
/*Contact me*/
/************/
	contactMe()

})


/*-------------------------------------------------*/

/*
* lance un audio avec plusieurs effets visuels
* tout reviens à la normale à la fin de l'audio 
*/
function konami(audio) {
	let html = document.querySelector("html")
	let rotation = 0
	let allSect = document.querySelectorAll(".screen")
	
	let inter = setInterval(function(){
		html.style.transform="rotate("+rotation+"deg)"
		rotation++
		if(rotation>180)
			clearInterval(inter)
		},20)
	let colors = setInterval(function(){
		for(let sect of allSect)
		{
			let r = Math.floor(Math.random()*255)
			let g = Math.floor(Math.random()*255)
			let b = Math.floor(Math.random()*255)
			sect.style.backgroundColor="rgb("+r+","+g+","+b+")"
		}
	},100)
	if(audio.paused)
	{
		audio.volume=0.2
 		audio.play()
 		console.log("***********************************************")
 		console.log("KONAMI | Music Among Us remix by MoondaiMusic : https://soundcloud.com/moondaimusic/among-us-moondai-remix")
 		console.log("***********************************************")
	}
	
	let timeAudioEnd=audio.duration*1000

	setTimeout(function(){
		rotation=180
		let inter = setInterval(function(){
			html.style.transform="rotate("+rotation+"deg)"
			rotation++
			if(rotation>360)
			{
				html.style.transform="rotate(0deg)"
				clearInterval(inter)
				clearInterval(colors)
				for(let sect of allSect)
				{
					sect.style.backgroundColor="transparent"
				}
			}
		},20)
	},timeAudioEnd)
}

/*
* Compare les classes de deux elements, renvoie true si ont une classe en commun
* Attention : but est de comparer un bouton pour trouver son equivalent, faire attention qu'ils ont bien qu'une classe entre eux deux commune
*/
function containClassOf(e,eTarget){
	for(let classs of eTarget.classList)
	{
		if(e.classList.contains(classs))
		{
			return true;
		}
	}
	return false;
}

/*
* Affiche le nav, en prenant en compte les resizes pour le responsive
* gere effet de fade in lorsqu'on est sur le top
*/
function displayNav()
{
	let navBtn=document.querySelector(".nav_btn")
	let nav=document.querySelector("nav")
	let changeWidth=750//px
	let actualWidth=document.querySelector("body").clientWidth+14
	let actualHeight=document.querySelector("#presentation_container").clientHeight

	window.addEventListener("resize",function(){
		actualWidth=document.querySelector("body").clientWidth+14
		if((actualWidth<changeWidth))
		{
			nav.classList.add("hidden")
			nav.style.backgroundColor='rgba(0,0,50,0.95)'
		}
		if(!(actualWidth<changeWidth || window.innerWidth<=changeWidth))
		{
			nav.classList.remove("hidden")
			nav.style.backgroundColor='rgba(0,0,50,'+window.scrollY/actualHeight+')';
			
		}
	})
	document.addEventListener("scroll", function()
	{
		if(actualWidth>=changeWidth)
		{

			nav.style.backgroundColor='rgba(0,0,50,'+window.scrollY/actualHeight+')';

		}
	})

	navBtn.addEventListener('click',function(){
		nav.classList.toggle("hidden")
	})

	nav.addEventListener("click", function(){
		if((actualWidth<changeWidth || window.innerWidth<=changeWidth))
		{
			nav.classList.add("hidden")//cache le menu quand un click sur mobile (pour afficher la page)
		}
	})
}

/*
* Animation des ronds skills (rond se dessine petit a petit selon % de compétence)
*/
function displaySkills(circleKey){

	let sectionCircle=document.querySelector("#skill_display_content").querySelector(circleKey)

	for(let allCircles of sectionCircle.querySelectorAll(".circle"))
	{		
		let percentage=allCircles.querySelector("span").innerHTML
		let circle = allCircles.querySelector(".secondCircle")
		let ratio=440
		let strokeDashOffset=ratio-(ratio*percentage)/100
		let start=ratio

		let i = setInterval(function(){
			if(start>strokeDashOffset)
			{
				circle.style.strokeDashoffset=start
				start--
			}
			else
			{
				clearInterval(i)
			}
		},5)
		
	}

}

/************/
/*Contact me*/
/************/
/*fonctions js_send et toParams copiées puis modifiées, trouvées sur stackoverflow.com/questions/7381150/how-to-send-an-email-from-javascript"*/
/*
* Gère l'envoie de mail et d'erreur
*/
function contactMe()
{
	var sendButton = document.querySelector("#mail_send")
	var form = "contact_form"
	var stat = document.querySelector("#mail_statut")

    var data_js = {
        "access_token": "" 
    };

    //reset le form et dit que mail est envoyé
    function formSuccess() {
        sendButton.value='Send'
        sendButton.disabled=false
        stat.innerHTML="&#10003; Mail sended successfully, I will answer you as soon as possible! &#10003;"
    	for(let anInput of document.querySelector("#"+form).querySelectorAll(".input_contact"))
    	{
    		anInput.value=''
    	}
    }

    //affiche error et/ou erreur du mail si existe
    function formError(error,email) {
        sendButton.value='Try again'
        sendButton.disabled=false
        if(email===true)//pas d'erreur mail, affiche rien
        	email=''
        else
        	email=" "+email
        stat.innerHTML="&#10008; Please complete all fields or try later (error: "+error+email+") &#10008;"
    }

    //expression pour verifier si un email est bien formaté
    function ValidateEmail(email) 
	{
	 if (/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(email))
	  {
	    return (true)
	  }
	    return "Invalid email"
	}

	//gere l'envoi
    function js_send() {
        sendButton.value='Sending…'
        sendButton.disabled=true
        var request = new XMLHttpRequest()
        var email=document.querySelector("#" + form + " [name='email']").value

        request.onreadystatechange = function() {
            if (request.readyState == 4 && request.status == 200 && ValidateEmail(email)===true) {
                formSuccess()
            } else
            if(request.readyState == 4) {
                formError(request.response, ValidateEmail(email))
            }
        };

        var subject = document.querySelector("#" + form + " [name='object']").value
        var message = document.querySelector("#" + form + " [name='msg']").value
        data_js['subject'] = subject

        if(message!='' && message!=" " && message!=null)//pour garder la verification sur message tout en incluant le mail (par réussi à ajouter autrement)
        {
        	message="Email: "+email.toLowerCase()+"\n\n\n"+message+"\n\n\nMail sended by my portfolio: "+window.location.href.split(/[?#]/)[0]//sans anchor ni param
        }
        
        data_js['text'] = message

        var params = toParams(data_js)

        let validateMail=ValidateEmail(email)
       /* if(validateMail===true)
        {
        	request.open("POST", "https://postmail.invotes.com/send", true)
	        request.setRequestHeader("Content-type", "application/x-www-form-urlencoded")
	        request.send(params)
        }
        else
        	formError('', validateMail)
        */

        return false
    }

    sendButton.onclick = js_send

    function toParams(data_js) {
        var form_data = []
        for ( var key in data_js ) {
            form_data.push(encodeURIComponent(key) + "=" + encodeURIComponent(data_js[key]))
        }

        return form_data.join("&")
    }

    var js_form = document.getElementById(form)
    js_form.addEventListener("submit", function (e) {
        e.preventDefault()
    });
}
/*
*actualise les positions des etoiles et la taille de la canvas selon l'ecran (resize)
*/
function refreshStars(canvas,c,balls){
	c = canvas.getContext("2d")
    canvas.width = document.querySelector("body").clientWidth-15
    canvas.height = window.innerHeight

    w = canvas.width
    h = canvas.height
    for(ball of balls)
    {
      ball.x=Math.random()*w
      ball.y=Math.random()*h
    }
 }

function _(tag, content, parent, id=null, classs=null) {
	let element = document.createElement(tag)
	if (content)
		element.appendChild(document.createTextNode(content))
	if (id)
		element.id = id
	if (classs)
		element.classList.add(classs)
	parent.appendChild(element)
	return element
}