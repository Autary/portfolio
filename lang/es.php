<?php 

define("LANG", "es");
define("DLCVLINK","CV-Nicolas_Wadoux-International");
define("METADESC","Portafolio creado por Nicolas Wadoux. Estudiante de informática, quiero especializarme en desarrollo web y móvil. Actualmente estoy en un contrato de formación y aprendizaje. Competencias: html, css, php, js, sql, java, rgpd, android, swift, laravel, symfony.");
define("MAINTITLE","Portafolio | Nicolas wadoux - Estudiante de Informática Web y Móvil");

/*NAV*/

define("ABOUTME", "Sobre mi");
define("COMPETENCES", "Competencias");
define("QUALIFICATIONS", "Diplomas");
define("PORTFOLIO", "Portafolio");
define("REFERENCES", "Referencias");
define("CONTACTME", "Contacto");
define("CV", "CV");

/*HOME*/

define("HOME_STATUS", "Estudiante de Informática Web y Móvil");

/*ABOUT ME*/

define("PRESENTATION_BTN", "Presentación");
define("LANGUAGES_BTN", "Idiomas");
define("SOFTSKILLS_BTN", "Soft skills");
define("HOBBIES_BTN", "Pasiones");

define("PRESENTATION_TXT_1", "Actualmente en un Licenciatura profesional Desarrollo Informática Multisoporte, en trabajo-estudio, soy un estudiante con el deseo de especializarme en desarrollo web y móvil. Enfoco mis estudios en esta área, guiado por mi deseo de aprender y mi curiosidad. Como apasionado, me gusta participar activamente en las actividades de una empresa, pero también crear proyectos personales, con el objetivo de aumentar mis habilidades.");
define("PRESENTATION_TXT_2", "El desarrollo web y móvil apareció como algo obvio durante mi educación. Crear aplicaciones web cada vez más sostenibles, preocupadas por la experiencia del usuario, es muy importante para mí. Las tecnologías web y móvil están cambiando nuestras vidas, y quiero ser parte de este cambio.");
define("PRESENTATION_TXT_3", "Si quieres aprender más sobre mí, o contactarme, ¡puedes explorar este sitio o descargar mi CV! :) :)");

define("LANGUAGES_FR", "Francés");
define("LANGUAGES_FR_LVL", "Langua nativa");
define("LANGUAGES_EN", "Inglés");
define("LANGUAGES_EN_LVL", "Intermedio-Avanzado (B2)");
define("LANGUAGES_ES", "Español");
define("LANGUAGES_ES_LVL", "Intermedio-Avanzado (B1+)");

define("SOFTSKILLS_EMOTION", "<h3>Inteligencia emocional</h3>: Soy una persona empática, simpática y siempre optimista.");
define("SOFTSKILLS_RELATION", "<h3>Inteligencia de relaciones</h3>: Espíritu de equipo, siempre actúo para mantener una cooperación positiva.");
define("SOFTSKILLS_CURIOSITY", "<h3>Curiosidad</h3>: Diempre me gusta aprender más sobre todos los temas");
define("SOFTSKILLS_PONCTUALITY", "<h3>Puntualidad</h3> y buena <h3>gestión del tiempo</h3>");
define("SOFTSKILLS_RESOLUTION", "<h3>Capacidad resolutiva</h3>: Me gusta encontrar la solución más sencilla para responder a una necesidad, ya sea solo o en equipo");

define("HOBBIES_VIOLIN_TILTE", "Tocar el violín");
define("HOBBIES_VIOLIN_1", "Toco el violín desde 2016.");
define("HOBBIES_VIOLIN_2", "He participado en obras de caridad como \"Octobre rose\" (Octubre rosa), o en el escenario, durante las comidas para personas mayores durante las vacaciones de Navidad, ¡y también en conciertos en bares!");
define("HOBBIES_TRAVEL_TITLE", "Viajar / La cultura");
define("HOBBIES_TRAVEL", "Me gustaría viajar por todo el mundo algún día. Me fascinan otros países y su propia cultura. Sin olvidar la cultura culinaria ;p");
define("HOBBIES_COOKING_TITLE", "Pastelería");
define("HOBBIES_COOKING", "Hablando de comida, ¿a quién no le gusta los pasteles? Troncos de Navidad, verines, pastelito, macarrones, ¡me encanta hacer pasteles!");

/*COMPETENCES*/

define("WEB","Web");
define("DB","Base de datos");
define("PROGRAMMING","Programación");
define("OTHERS","Otros");
define("GDPR","RGPD");

/*QUALIFICATIONS*/

define("LP_DIM", "Licenciatura profesional");
define("DIM", "Licenciatura profesional Desarrollo Informática Multisoporte en un contrato de formación y aprendizaje (60 ECTS, con 120 ECTS previamente validados)");
define("DIM_UNIV", "Universidad Savoie Mont-Blanc - IUT Annecy-Le-Vieux & L'école by CCI - Annecy, Francía");
define("DUT", "DUT Informatique");
define("DUT_INFO", "Diploma universitario en informática (120 ECTS)");
define("DUT_UNIV", "Universidad Savoie Mont-Blanc - IUT Annecy-Le-Vieux, Francía");
define("BAC_S", "Bachillerato Científico");
define("BAC_S_DESC", "Diploma especializado en ciencas - Calificación Notable (70-80%)");
define("BAC_OPT", "Opciones : ISN (Informaticá y ciencas del numérico) y Música");
define("BAC_UNIV", "Instituto Guillaume Fichet - Bonneville, Francía");

/*PORTFOLIO*/

define("PERSONAL","Proyectos personales");
define("PROFESSIONAL","Proyectos profesionales");
define("SEEIT","Ir a ver");

define("KILI_HEADER","Kili");
define("KILI_CONTENT_P1","Proyecto tutelado durante mi DUT.");
define("KILI_ALT","Imagen de representación de Kili");
define("KILI_CONTENT_P2","En equipo de 5, debíamos diseñar y realizar un sitio de intercambio de libros (diseño, BDD, modelos, pliego de condiciones, ...)");
define("KILI_CONTENT_P3","Gracias a AdobeXD, creamos el diseño del sitio en Mobile First.");
define("KILI_CONTENT_P4","Como jefe de proyecto, he tenido que mantener el contacto con el cliente y coordinar nuestro equipo.");

define("PORTFOLIO_HEADER","Portafolio V1");
define("PORTFOLIO_ALT","Imagen de representación del portafolio");
define("PORTFOLIO_CONTENT_1","Un portafolio responsivo creado por un trabajo de programación y inglés, sin Framework o librería.");
define("PORTFOLIO_CONTENT_2","Creado en 30 horas.");

define("TRIPAD_HEADER","TripAdvisor alike");
define("TRIPAD_ALT","Imagen de representación de TripAdvisor alike");
define("TRIPAD_CONTENT_1","Una versión simplificada de TripAdvisor, en grupo de 5, con el objetivo de implementar el méthodo ágil Scrum, en 4 esprintes de 4 horas.");
define("TRIPAD_CONTENT_2","Además, este proyecto sirvió para aprender a usar Laravel y las buenas prácticas de MVC.");

define("INFOMANIAK_HEADER","Taquilla Infomaniak");
define("INFOMANIAK_DATE","01/04/2021 - Hoy");
define("INFOMANIAK_ALT","Imagen de representación de la Taquilla de Infomaniak");
define("INFOMANIAK_CONTENT_1","Trabajé para la taquilla de Infomaniak, en un equipo de 6, durante mis prácticas y el programa de estudio y trabajo.");
define("INFOMANIAK_CONTENT_2","Este producto permite a los organizadores crear, vender, administrar y controlar sus eventos, desarrollado con Laravel, Stencil, AngularJS y Angular.");
define("INFOMANIAK_CONTENT_3","Pude, por ejemplo, trabajar en la configuración de promociones, una API con torniquetes, intercambio de boletos, etc.");

define("ZONESKI_HEADER","ZoneSki");
define("ZONESKI_ALT","Imagen de representación de Zoneski");
define("ZONESKI_CONTENT_DATE","01/2019, durante una semena");
define("ZONESKI_CONTENT_1","Primer premio de un concurso.");
define("ZONESKI_CONTENT_2","En equipo de 6, solamente después 4 meses de formación en desarrollo, tuvimos que crear un sitio web desde cero.");
define("ZONESKI_CONTENT_3","Debíamos prestar atención a la semiótica, la comunicación y la redacción de las páginas.");

define("TOQUES_HEADER","La Toquerie");
define("TOQUES_ALT","Imagen de representación de la Toquerie");
define("TOQUES_DATE","En desarrollo");
define("TOQUES_CONTENT_P1","Un proyecto con el objetivo de aprender las mejores prácticas para desarrollar una API Laravel y un cliente Angular.");
define("TOQUES_CONTENT_P2","La idea de este proyecto se resume en un sitio que permita buscar recetas, siendo apropiado para varios criterios. También será posible compartir recetas con un grupo de usuarios, como compartir recetas en familia, permitiendo así la creación de recetarios digitales.");

define("PORTFOLIO2_HEADER","Portafolio v2");
define("PORTFOLIO2_ALT","Imagen de representación de este portafolio");
define("PORTFOLIO2_CONTENT_P1","Un nuevo portafolio totalmente responsivo. Con este sitio, aprendí a usar BootStrap.");
define("PORTFOLIO2_CONTENT_P2","El segundo objetivo de este sitio era crear un modelo en AdobeXD y luego crear el sitio con la mayor fidelidad posible.");
define("PORTFOLIO2_CONTENT_P3","Finalmente, este sitio me permitió configurar un sistema multilingüe. Pero también para administrar su indexación.");
define("PORTFOLIO2_DATE","Todavía en desarrollo");
define("PORTFOLIO2_LINK","Veer la maqueta");

define("ETUCUISINE_HEADER","Etu'Cuisine");
define("ETUCUISINE_ALT","Imagen de representación de Etu'cuisine");
define("ETUCUISINE_CONTENT_P1","Mi primer proyecto, un sitio de recetas para estudiantes, conectado con una base de datos.");
define("ETUCUISINE_CONTENT_P2","Este sitio se utilizó para aprender a diseñar una base de datos y utilizarla con PHP.");

define("JSNATIVE","JS Nativo");

/*REFERENCES*/

define("JOB1REF","Profesora de inglés");
define("WORK1REF","Comunicar y trabajar en inglés");
define("LOC1REF","IUT Annecy, USMB, Francía");

define("JOB2REF","Profesor de informaticá");
define("WORK2REF","Base de datos y arquitectura de software");
define("LOC2REF","IUT Annecy, USMB, Francía");

/*CONTACT ME*/

define("GETINTOUCH_P1","¿Un proyecto? ¿En reclutamiento? ¿Quieres saludar?");
define("GETINTOUCH_P2","¡No lo dudes más, entramos en contacto!");
define("PERSONALDATA","Datos personales");
define("EMAIL","Tu correo electrónico");
define("OBJECTMSG","Asunto");
define("MESSAGE","Mensaje");
define("SEND","Enviar");
define("DLCV","Descargas mi CV aquí");
define("MAILSUCCESSFULYSENDED","¡Correo enviado con éxito!, Le contactaré pronto");
define("MAILSENDINGERROR","Un error se ha producido, por favor rellena todos los campos");
define("GDPRCONTACT", "De acuerdo con el RGPD (Reglamento General de Protección de Datos), los datos recolectados por este formulario solamente serven para ponerse en contacto conmigo, por correo electrónico. Podría mantener nuestro futuro intercambio de correo electrónico y los datos personales relacionados. El único propósito de estos datos es ponerse en contacto e intercambiar. Nunca los datos recibidos por este formulario o por nuestros futuros intercambios serán utilizados aparte de este contexto, sin su consentimiento. Tiene un derecho de supresión sobre estos correos electrónicos y los datos relacionados en cualquier momento. Por eso, solamente debe contactarme para que suprimo todos nuestros intercambios.");