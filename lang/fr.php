<?php 

define("LANG", "fr");
define("DLCVLINK","CV-Nicolas_Wadoux-Français");
define("METADESC","Portfolio créé par Nicolas Wadoux. Etudiant en informatique, je souhaite me spécialiser en développement web et mobile. Je suis actuellement en alternance. Compétences : html, css, php, js, sql, java, rgpd, android, swift, laravel, symfony.");
define("MAINTITLE","Portfolio | Nicolas wadoux - Étudiant Informatique Web et Mobile");

/*NAV*/

define("ABOUTME", "À propos");
define("COMPETENCES", "Compétences");
define("QUALIFICATIONS", "Diplômes");
define("PORTFOLIO", "Portfolio");
define("REFERENCES", "Références");
define("CONTACTME", "Me contacter");
define("CV", "CV");

/*HOME*/

define("HOME_STATUS", "Étudiant Informatique Web et Mobile");

/*ABOUT ME*/

define("PRESENTATION_BTN", "Présentation");
define("LANGUAGES_BTN", "Langues");
define("SOFTSKILLS_BTN", "Soft skills");
define("HOBBIES_BTN", "Passions");

define("PRESENTATION_TXT_1", "Actuellement en licence professionnelle Développeur Informatique Multisupport, en alternance, je suis un jeune étudiant souhaitant se spécialisant dans le développement web et mobile. J’oriente mes études dans ce domaine, guidé par mon désir d'apprendre et ma curiosité. En tant que passionné, j’aime participer activement aux activités d’une entreprise, mais aussi créer des projets personnels, dans le but d'augmenter en compétences.");
define("PRESENTATION_TXT_2", "Le développement web et mobile est apparu comme une évidence lors de mes formations. Créer des applications web toujours plus durables, soucieuses de l'experience utilisateur et de plus en plus performantes, est un enjeu actuel très important et d'avenir. Le mobile change notre quotidien, il y prend une place de plus en plus importante. Je veux prendre part à ce changement.");
define("PRESENTATION_TXT_3", "Si vous souhaitez en apprendre plus sur moi, ou me contacter, n'hésitez plus, vous pouvez explorer ce site ou télécharger mon CV ! :)");

define("LANGUAGES_FR", "Français");
define("LANGUAGES_FR_LVL", "Langue Maternelle");
define("LANGUAGES_EN", "Anglais");
define("LANGUAGES_EN_LVL", "Niveau B2");
define("LANGUAGES_ES", "Espagnol");
define("LANGUAGES_ES_LVL", "Niveau B1+");

define("SOFTSKILLS_EMOTION", "<h3>Intelligence émotionnelle</h3> : Je suis quelqu'un d'empathique, sympathique et toujours optimiste");
define("SOFTSKILLS_RELATION", "<h3>Intelligence relationnelle</h3> : Esprit d'équipe, agit toujours pour maintenir une coopération positive");
define("SOFTSKILLS_CURIOSITY", "<h3>Curiosité</h3> : J'aime toujours en apprendre plus sur tous les sujets");
define("SOFTSKILLS_PONCTUALITY", "<h3>Ponctualité</h3> et bonne <h3>gestion du temps</h3>");
define("SOFTSKILLS_RESOLUTION", "<h3>Capacité de résolution </h3> : J'aime trouver la solution la plus simple pour répondre à un besoin, que ce soit seul ou en équipe");

define("HOBBIES_VIOLIN_TILTE", "Jouer du violon");
define("HOBBIES_VIOLIN_1", "Je joue du violon depuis 2016.");
define("HOBBIES_VIOLIN_2", "J'ai participé à des œuvres de charité comme \"Octobre Rose\", ou encore sur scène, lors de repas seniors durant les fêtes de noël, mais aussi des concerts dans des bars !");
define("HOBBIES_TRAVEL_TITLE", "Voyager / Culture");
define("HOBBIES_TRAVEL", "J'aimerais un jour faire le tour du monde, les autres pays et leur culture me passionnent. Sans oublier la culture culinaire ;p");
define("HOBBIES_COOKING_TITLE", "Pâtisserie");
define("HOBBIES_COOKING", "En parlant de nourriture, qui n'aime pas les gâteaux ? Buches, verrines, choux, macarons, j'adore pâtisser !");

/*COMPETENCES*/

define("WEB","Web");
define("DB","Base de données");
define("PROGRAMMING","Programmation");
define("OTHERS","Autres");
define("GDPR","RGPD");

/*QUALIFICATIONS*/

define("LP_DIM", "Licence Professionnelle DIM");
define("DIM", "Licence Développeur Informatique Multisupport en alternance (bac +3)");
define("DIM_UNIV", "Université Savoie Mont-Blanc - IUT Annecy-Le-Vieux & L'école by CCI - Annecy, France");
define("DUT", "DUT Informatique");
define("DUT_INFO", "Diplôme Universitaire d'Informatique (bac +2)");
define("DUT_UNIV", "Université Savoie Mont-Blanc - IUT Annecy-Le-Vieux, France");
define("BAC_S", "Baccalauréat Scientifique");
define("BAC_S_DESC", "Diplôme spécialisé en sciences - Mention Bien");
define("BAC_OPT", "Options : ISN (Informatique et Sciences du Numérique) et Musique");
define("BAC_UNIV", "Lycée Guillaume Fichet - Bonneville, France");

/*PORTFOLIO*/

define("PERSONAL","Projets personnels");
define("PROFESSIONAL","Projets professionnels");
define("SEEIT","Aller voir");

define("KILI_HEADER","Kili");
define("KILI_CONTENT_P1","Projet tutoré dans le cadre de mon DUT");
define("KILI_ALT","Image représentation de Kili");
define("KILI_CONTENT_P2","En équipe de 5, nous devions concevoir et réaliser un site d'échange de livres (design, BDD, modèles, Cahier des charges, ...)");
define("KILI_CONTENT_P3","Grâce à AdobeXD, nous avons créé le design du site en Mobile First.");
define("KILI_CONTENT_P4","En tant que chef de projet, j'ai dû maintenir le contact avec le client et coordonner notre équipe.");

define("PORTFOLIO_HEADER","Portfolio V1");
define("PORTFOLIO_ALT","Image représentation du portfolio");
define("PORTFOLIO_CONTENT_1","Un portfolio responsive créé pour un travail mêlant anglais et programmation, avec obligation de n'utiliser ni framework ni librairie.");
define("PORTFOLIO_CONTENT_2","Créé en environ 30h.");

define("TRIPAD_HEADER","TripAdvisor alike");
define("TRIPAD_ALT","Image représentation TripAdvisor alike");
define("TRIPAD_CONTENT_1","Une version simplifiée de TripAdvisor, en groupe de 5 lors d'un cours, ayant pour but de mettre en place la méthode agile Scrum, en 4 sprints de 4 heures.");
define("TRIPAD_CONTENT_2","Par la même occasion, ce projet a servit à apprendre à utiliser Laravel et les bonnes pratiques du MVC.");

define("INFOMANIAK_HEADER","Billetterie Infomaniak");
define("INFOMANIAK_DATE","01/04/2021 - Aujourd'hui");
define("INFOMANIAK_ALT","Image représentation Billetterie Infomaniak");
define("INFOMANIAK_CONTENT_1","Dans le cadre du stage en DUT, puis de l'alternance, j'ai travaillé chez Infomaniak, pour le produit Billetterie, dans une équipe de 6.");
define("INFOMANIAK_CONTENT_2","Ce produit permet à des organisateurs de créer, vendre, gérer et controller leurs évènements, sur une application Laravel, Stencil, AngularJS et Angular.");
define("INFOMANIAK_CONTENT_3","J'ai pu, par exemple, travailler sur la mise en place de promotions, d'une API avec des tourniquets, d'échange de billets, etc.");

define("ZONESKI_HEADER","ZoneSki");
define("ZONESKI_ALT","Image représentation Zoneski");
define("ZONESKI_CONTENT_DATE","01/2019, pendant une semaine");
define("ZONESKI_CONTENT_1","Premier prix d'une semaine concours.");
define("ZONESKI_CONTENT_2","En équipe de 6, après seulement 4 mois de formation en développement, nous avons dû créer un site web de A à Z.");
define("ZONESKI_CONTENT_3","Nous devions faire attention à la sémiotique, à la communication et à la rédaction des pages.");

define("TOQUES_HEADER","La Toquerie");
define("TOQUES_ALT","Image représentation de la Toquerie");
define("TOQUES_DATE","En cours de développement");
define("TOQUES_CONTENT_P1","Un projet ayant pour but d'apprendre les bonnes pratiques pour le développement d'une API Laravel et d'un client Angular.");
define("TOQUES_CONTENT_P2","L'idée de ce projet se résume en un site permettant de trouver des recettes, convenant à différents critères. Il sera aussi possible de partager des recettes avec un groupe d'utilisateurs, comme pour le partage de recettes familiales, permettant ainsi la création de livre de recettes numériques.");

define("PORTFOLIO2_HEADER","Portfolio v2");
define("PORTFOLIO2_ALT","Image représentation de ce portfolio");
define("PORTFOLIO2_CONTENT_P1","Un nouveau portfolio totalement responsive, grâce auquel j'ai appris à utiliser BootStrap.");
define("PORTFOLIO2_CONTENT_P2","Le deuxième but de ce portfolio était de créer une maquette sur AdobeXD puis réaliser le plus fidèlement possible le site.");
define("PORTFOLIO2_CONTENT_P3","Enfin, ce site m'a permis de mettre en place un système multilingue. Mais aussi de gérer son référencement.");
define("PORTFOLIO2_DATE","Toujours en cours de développement");
define("PORTFOLIO2_LINK","Voir la maquette");

define("ETUCUISINE_HEADER","Etu'Cuisine");
define("ETUCUISINE_ALT","Image représentation d'Etu'cuisine");
define("ETUCUISINE_CONTENT_P1","Mon tout premier projet, un site répertoriant plusieurs recettes pour les étudiants en lien avec une base de données.");
define("ETUCUISINE_CONTENT_P2","Ce site à servit à apprendre à concevoir une base de donnée et à l'utiliser avec PHP.");

define("JSNATIVE","JS Natif");

/*REFERENCES*/

define("JOB1REF","Professeure d'anglais");
define("WORK1REF","Communiquer et travailler en anglais");
define("LOC1REF","IUT Annecy, USMB, France");

define("JOB2REF","Professeur d'informatique");
define("WORK2REF","Base de données, architecture logicielle");
define("LOC2REF","IUT Annecy, USMB, France");

/*CONTACT ME*/

define("GETINTOUCH_P1","Un projet ? En plein recrutement ? Envie de dire bonjour ?");
define("GETINTOUCH_P2","N'hésitez plus, entrons en contact !");
define("PERSONALDATA","Données personnelles");
define("EMAIL","Votre mail");
define("OBJECTMSG","Objet");
define("MESSAGE","Message");
define("SEND","Envoyer");
define("DLCV","Téléchargez ici mon CV");
define("MAILSUCCESSFULYSENDED","Mail envoyé avec succès ! Je vous contacterai dans la semaine");
define("MAILSENDINGERROR","Une erreur est survenue, merci de bien renseigner tous les champs");
define("GDPRCONTACT", "Conformément au RGPD, les données collectées par ce formulaire servent uniquement afin d'entrer en contact, par mail. Je pourrais être amené à conserver notre prochain échange de mail et les données personnelles afférentes. L'unique finalité de ces données est bien d'entrer en contact et d'échanger. Jamais les données reçues par le biais de ce formulaire ou de nos futurs échanges ne seront utilisées hors de ce contexte, sans votre consentement. Vous bénéficiez d'un droit de suppression de ces mails et de leurs données à tous moments. Il suffit pour cela de me contacter afin que je supprime tous nos échanges.");















 ?>