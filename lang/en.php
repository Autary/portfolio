<?php 

define("LANG", "en");
define("DLCVLINK","CV-Nicolas_Wadoux-International");
define("METADESC","Portfolio created by Nicolas Wadoux. Computer science student, I want to specialize in web and mobile development. I am currently in a block release training. Skills: html, css, php, js, sql, java, gdpr, android, swift, laravel, symfony.");
define("MAINTITLE","Portfolio | Nicolas wadoux - IT Student Web and Mobile");

/*NAV*/

define("ABOUTME", "About me");
define("COMPETENCES", "Competences");
define("QUALIFICATIONS", "Qualifications");
define("PORTFOLIO", "Portfolio");
define("REFERENCES", "References");
define("CONTACTME", "Contact me");
define("CV", "CV");

/*HOME*/

define("HOME_STATUS", "IT Student, Web and Mobile");

/*ABOUT ME*/

define("PRESENTATION_BTN", "Presentation");
define("LANGUAGES_BTN", "Languages");
define("SOFTSKILLS_BTN", "Soft skills");
define("HOBBIES_BTN", "Hobbies");

define("PRESENTATION_TXT_1", "Currently in a vacational degree regarding multisupport IT development, in work-study, I am a young student wishing to specialize in web and mobile development. I focus my studies in this area, guided by my desire to learn and my curiosity. As a passionate, I like to actively participate in the activities of a company, but also create personal projects, with the aim of increase my skills.");
define("PRESENTATION_TXT_2", "Web and mobile development appeared as obvious during my schooling. Creating ever more sustainable web applications, concerned about the user experience, is very important for me. Web and mobile technologies are changing our lives, and I want to be part of this change.");
define("PRESENTATION_TXT_3", "If you want to learn more about me, or get in touch, don't hesitate! You can explore this website nor download my CV! :)");

define("LANGUAGES_FR", "French");
define("LANGUAGES_FR_LVL", "Mother tongue");
define("LANGUAGES_EN", "English");
define("LANGUAGES_EN_LVL", "Upper Intermediate (B2)");
define("LANGUAGES_ES", "Spanish");
define("LANGUAGES_ES_LVL", "Upper Intermediate (B1+)");

define("SOFTSKILLS_EMOTION", "<h3>Emotional intelligence</h3>: I am empathic, friendly and always optimistic");
define("SOFTSKILLS_RELATION", "<h3>Relationship intelligence</h3>: Team spirit, always acts to maintain positive cooperation");
define("SOFTSKILLS_CURIOSITY", "<h3>Curiosity</h3>: I always like to learn more about every possible subjects");
define("SOFTSKILLS_PONCTUALITY", "<h3>Punctuality</h3> and good <h3>time management</h3>");
define("SOFTSKILLS_RESOLUTION", "<h3>Resolving ability</h3>: I like to find the simplest solution to meet a need, whether alone or teaming");

define("HOBBIES_VIOLIN_TILTE", "Play violin");
define("HOBBIES_VIOLIN_1", "I play violin since 2016.");
define("HOBBIES_VIOLIN_2", "I participated in charities such as \"Octobre rose\" (Pink October). Also on stage, during senior meals on Christmas holidays, and some concerts in bars!");
define("HOBBIES_TRAVEL_TITLE", "Travel / Culture");
define("HOBBIES_TRAVEL", "I would like to travel all around the world, other countries and their own culture fascinate me. Without forgetting the culinary culture ;p");
define("HOBBIES_COOKING_TITLE", "Baking");
define("HOBBIES_COOKING", "Speaking about food, who doesn't love cakes? Yule logs, verrines, choux buns, macaroons, I love baking!");

/*COMPETENCES*/

define("WEB","Web");
define("DB","Data Base");
define("PROGRAMMING","Programming");
define("OTHERS","Others");
define("GDPR","GDPR");

/*QUALIFICATIONS*/

define("LP_DIM", "Vocational degree");
define("DIM", "Vocational degree about multi-support IT in a block release training (60 ECTS, with 120 ECTS previously validated)");
define("DIM_UNIV", "University Savoy Mont-Blanc - IUT Annecy-Le-Vieux & L'école by CCI - Annecy, France");
define("DUT", "DUT Informatique");
define("DUT_INFO", "2-year university diploma in computer science (120 ECTS)");
define("DUT_UNIV", "University of Savoy Mont-Blanc - IUT Annecy-Le-Vieux, France");
define("BAC_S", "French Scientific baccalaureate");
define("BAC_S_DESC", "High School diploma specialised in science with honours (70-80%)");
define("BAC_OPT", "Options : ISN (IT and numerical science) & Music");
define("BAC_UNIV", "Guillaume Fichet High School - Bonneville, France");

/*PORTFOLIO*/

define("PERSONAL","Personal projects");
define("PROFESSIONAL","Professional projects");
define("SEEIT","Let's see it");

define("KILI_HEADER","Kili");
define("KILI_CONTENT_P1","A second year university tutored project.");
define("KILI_ALT","Representation picture of Kili");
define("KILI_CONTENT_P2","In team of 5, we had to design and develop a book exchange website (design, DB, models, specifications, ...)");
define("KILI_CONTENT_P3","Thanks to AdobeXD, we created the site's layout in Mobile First.");
define("KILI_CONTENT_P4","As the project manager, I handled the contact with the client and I had to coordinate our team.");

define("PORTFOLIO_HEADER","Portfolio V1");
define("PORTFOLIO_ALT","Representation picture of previous portfolio");
define("PORTFOLIO_CONTENT_1","A responsive portfolio created for an english and programming work, without Framework or Library.");
define("PORTFOLIO_CONTENT_2","Created in 30h.");

define("TRIPAD_HEADER","TripAdvisor alike");
define("TRIPAD_ALT","Representation picture of TripAdvisor alike");
define("TRIPAD_CONTENT_1","A simplified version of TripAdvisor, in team of 5. The goal was to use the scrum agile method, with 4 sprints of 4 hours.");
define("TRIPAD_CONTENT_2","At the same time, by this project I learned how to use Laravel and good MVC practices.");

define("INFOMANIAK_HEADER","Infomaniak Ticketing");
define("INFOMANIAK_DATE","01/04/2021 - Today");
define("INFOMANIAK_ALT","Representation picture of Infomaniak Ticketing");
define("INFOMANIAK_CONTENT_1","As part of the DUT internship, and then on work-study, I worked at Infomaniak, for the Ticketing product, in a team of 6.");
define("INFOMANIAK_CONTENT_2","This product allows organizers to create, sell, manage and control their events, on a Laravel, Stencil, AngularJS and Angular application.");
define("INFOMANIAK_CONTENT_3","For example, I worked on shop promotions, on an API with turnstiles, on ticket exchange, etc.");

define("ZONESKI_HEADER","ZoneSki");
define("ZONESKI_ALT","Representation picture of Zoneski");
define("ZONESKI_CONTENT_DATE","01/2019, during one week");
define("ZONESKI_CONTENT_1","1st prize of a web design week contest.");
define("ZONESKI_CONTENT_2","In team of 6, after only 4 months of schooling, we had to design from start to finish a website, about any topic we wanted.");
define("ZONESKI_CONTENT_3","We also had to pay attention to the semiotic and the writing.");

define("TOQUES_HEADER","La Toquerie");
define("TOQUES_ALT","Representation picture of la Toquerie");
define("TOQUES_DATE","Still under development");
define("TOQUES_CONTENT_P1","A project with the goal to learn best practices for developing a Laravel API and an Angular client.");
define("TOQUES_CONTENT_P2","The idea of this project is, in a nutshell, a site allowing to find receipts appropriated for various criteria. It will also be possible to share recipes with a group of users, sharing recipes in familly, allowing the creation of digital recipe books.");

define("PORTFOLIO2_HEADER","Portfolio v2");
define("PORTFOLIO2_ALT","Representation picture of this portfolio");
define("PORTFOLIO2_CONTENT_P1","A new fully responsive portfolio. Through it I learned how to use BootStrap.");
define("PORTFOLIO2_CONTENT_P2","The second goal of this portfolio was to create a layout with AdobeXD and then create the site as faithfully as possible.");
define("PORTFOLIO2_CONTENT_P3","Finally, this site allowed me to set up a multilingual system and manage its SEO.");
define("PORTFOLIO2_DATE","Still under development");
define("PORTFOLIO2_LINK","Go to the layout");

define("ETUCUISINE_HEADER","Etu'Cuisine");
define("ETUCUISINE_ALT","Representation picture of Etu'cuisine");
define("ETUCUISINE_CONTENT_P1","My very first project, a site listing several recipes for students, linked to a database.");
define("ETUCUISINE_CONTENT_P2","The goal of this site was to learn how to design a database and use it with PHP.");

define("JSNATIVE","JS Native");

/*REFERENCES*/

define("JOB1REF","English teacher");
define("WORK1REF","Communicate and work in english");
define("LOC1REF","IUT Annecy, USMB, France");

define("JOB2REF","It teacher");
define("WORK2REF","Database and software architecture");
define("LOC2REF","IUT Annecy, USMB, France");

/*CONTACT ME*/

define("GETINTOUCH_P1","A project? Recruiting? Just to say hello?");
define("GETINTOUCH_P2","Don't hesitate, let's get in touch!");
define("PERSONALDATA","Personal data");
define("EMAIL","Your email");
define("OBJECTMSG","Object");
define("MESSAGE","Message");
define("SEND","Send");
define("DLCV","Download my résumé");
define("MAILSUCCESSFULYSENDED","Email successfully sended! I will contact you soon");
define("MAILSENDINGERROR","An error occurred, please fill in all fields");
define("GDPRCONTACT", "In compliance with the GDPR, personal data collected by this form are only used to get in touch with you, by email. I could keep our futur email exchange and related personal data. The only one purpose of this data is to get in touch and exchange. I will never use your data received by this form or by our futur exchange apart from this context, without your consent. You hold a right of deletion concerning these emails and data at any time. All you have to do is contact me and I will delete all our exchanges and related data.");